﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemoryReader {
    class ByteData {
        public string Addr { get; set; }
        public string Byte { get; set; }
        public char Ascii { get; set; }

        /// <summary>
        /// Fills the default data based on the given byte and address
        /// </summary>
        /// <param name="addr"></param>
        /// <param name="b"></param>
        public ByteData(IntPtr addr, byte? b){
            //Converts IntPtr to hexadecimal string
            Addr = addr.ToString("X8");

            //Converts byte to ascii character. If the character is non-printable, then it will be replaced with a dot.
            Ascii = (b > 0x1F && b < 0x7F) ? (char)b : '.';

            //Converts a nullable byte to a string representation of it. If the byte is null, then it will be replaced with two questionmarks.
            Byte = b == null ? "??" : ((byte)b).ToString("X2");
        }
    }
}
