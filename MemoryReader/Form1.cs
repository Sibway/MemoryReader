﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MemoryReader {

    public partial class MemoryReader : Form {

        private Memory m { get; set; } = new Memory();
        private IntPtr startAddr { get; set; }
        private int bAmnt { get; set; } = 16;
        private int rAmnt { get; set; } = 1000;
        private int lineBufferSize { get; set; } = 100;


        /// <summary>
        /// Sets some default values for startAddr and bAmnt. And reloads the MemoryView window
        /// </summary>
        public MemoryReader() {
            InitializeComponent();

            startAddr = m.BaseAddr;
            bAmnt = int.Parse((string)bprComboBox.SelectedItem);

            RefreshMemoryView();
        }

        /// <summary>
        /// Async method to read a line of bytes
        /// </summary>
        /// <param name="addr">Address to start reading from</param>
        /// <returns>Will return a line of bytes as string. divided in start address, bytes and the ascii representation of the bytes</returns>
        private async Task<string> ReadLineAsync(IntPtr addr) {
            ByteData[] byteDatas = await m.ReadBytesAsync(addr, bAmnt);
            string line = "";
            line += byteDatas.First().Addr + " ";
            foreach (ByteData bd in byteDatas) {
                line += bd.Byte + " ";
            }
            foreach (ByteData bd in byteDatas) {
                line += bd.Ascii;
            }
            line += Environment.NewLine;
            return line;
        }
        
        /// <summary>
        /// Async method to display a certain amount of lines with information about bytes.
        /// Will read multiple lines in parallel to increase efficiency. The amount of lines to be read in parallel is controlled by the lineBufferSize property.
        /// </summary>
        /// <param name="lines"></param>
        /// <returns></returns>
        private async Task DisplayLines(int lines) {
            IntPtr newStartAddr = startAddr;
            for (int i = 0; i < rAmnt/lineBufferSize; i++) {
                Task<string>[] lineStrings = new Task<string>[lineBufferSize];
                for (int j = 0; j < lineBufferSize; j++) {
                    lineStrings[j] = ReadLineAsync(newStartAddr);
                    newStartAddr += bAmnt;
                }
                string[] strings = await Task.WhenAll(lineStrings);
                MemoryView.Text += string.Concat(strings);
            }

        }

        /// <summary>
        /// Will reload the MemoryView window after the reload button has been clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReloadButton_Click(object sender, EventArgs e) {
            RefreshMemoryView();
        }


        /// <summary>
        /// Will reload the MemoryView window and changes the amount of bytes visible per row to the amount selected in bprComboBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bprComboBox_SelectedValueChanged(object sender, EventArgs e) {
            bAmnt = int.Parse((string)bprComboBox.SelectedItem);
            RefreshMemoryView();
        }

        /// <summary>
        /// Will clear the MemoryView window and refill it using DisplayLines and changes the last selected address to the current startaddress.
        /// </summary>
        /// <returns></returns>
        private async Task RefreshMemoryView() {
            string lastAddr = "0x" + startAddr.ToString("X8");
            ObjectPicker.Items.Add(lastAddr);
            ObjectPicker.Text = lastAddr;
            MemoryView.Clear();
            await DisplayLines(rAmnt);
        }

        /// <summary>
        /// Runs ObjectPickerChanged window after the enter button is hit while in the ObjectPicker combobox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ObjectPicker_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter) return;
            ObjectPickerChanged();
        }

        /// <summary>
        /// Runs ObjectPickerChanged after the select value has changed and if there is an element other than the default one in the combobox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ObjectPicker_SelectedValueChanged(object sender, EventArgs e) {
            if (ObjectPicker.Items.Count < 2) return;
            ObjectPickerChanged();
        }

        /// <summary>
        /// Will reload the MemoryView window and adds the current startaddress to the ObjectPicker combobox
        /// </summary>
        private void ObjectPickerChanged() {
            string objectPickerTxt = ObjectPicker.Text.Trim();
            if (objectPickerTxt == "") startAddr = m.BaseAddr;
            else startAddr = (IntPtr)Convert.ToInt32(ObjectPicker.Text.Trim(), 16);
            RefreshMemoryView();
        }
    }
}
