﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace MemoryReader {
    class Memory {

        /// <summary>
        /// Returns the base address of this process
        /// </summary>
        public IntPtr BaseAddr { get { return Process.GetCurrentProcess().MainModule.BaseAddress; } }

        /// <summary>
        /// Asynchronously reads a given amount of bytes and returns their data as a ByteData array. The bytes will be read in parallel for extra efficiency
        /// </summary>
        /// <param name="addr">Address to start reading bytes from</param>
        /// <param name="bytesToRead">The amount of bytes to read</param>
        /// <returns></returns>
        public async Task<ByteData[]> ReadBytesAsync(IntPtr addr, int bytesToRead) {
            List<Task<ByteData>> tBD = new List<Task<ByteData>>();

            for (int i = 0; i < bytesToRead; i++) {
                tBD.Add(ReadByteAsync(addr + i));
            }
            return await Task.WhenAll(tBD);
        }

        /// <summary>
        /// Runs the ReadByte method asynchronously
        /// </summary>
        /// <param name="addr">Address to read byte from</param>
        /// <returns></returns>
        public async Task<ByteData> ReadByteAsync(IntPtr addr) {
            ByteData bd = await Task.Run(() => ReadByte(addr));
            return bd;
        }

        /// <summary>
        /// Reads a single byte at the given address. And returns its data as a ByteData object.
        /// Has a HandleProcessCorruptedStateExceptions attribute to allow catching of "System.AccessViolationException" exceptions 
        /// </summary>
        /// <param name="addr">Address to read byte from</param>
        /// <returns>Returns a ByteData object</returns>
        [HandleProcessCorruptedStateExceptions]
        private ByteData ReadByte(IntPtr addr) {
            byte? b;
            try { b = Marshal.ReadByte(addr); }
            catch { b = null; }

            return new ByteData(addr, b);
        }
    }
}
