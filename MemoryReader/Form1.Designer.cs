﻿namespace MemoryReader {
    partial class MemoryReader {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MemoryReader));
            this.AddressLabel = new System.Windows.Forms.Label();
            this.MemoryView = new System.Windows.Forms.RichTextBox();
            this.ObjectPicker = new System.Windows.Forms.ComboBox();
            this.ReloadButton = new System.Windows.Forms.Button();
            this.bprComboBox = new System.Windows.Forms.ComboBox();
            this.ColumnsLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // AddressLabel
            // 
            this.AddressLabel.AutoSize = true;
            this.AddressLabel.Location = new System.Drawing.Point(12, 9);
            this.AddressLabel.Name = "AddressLabel";
            this.AddressLabel.Size = new System.Drawing.Size(48, 13);
            this.AddressLabel.TabIndex = 1;
            this.AddressLabel.Text = "Address:";
            // 
            // MemoryView
            // 
            this.MemoryView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MemoryView.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MemoryView.Location = new System.Drawing.Point(13, 32);
            this.MemoryView.Name = "MemoryView";
            this.MemoryView.ReadOnly = true;
            this.MemoryView.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.MemoryView.Size = new System.Drawing.Size(775, 406);
            this.MemoryView.TabIndex = 2;
            this.MemoryView.Text = "";
            this.MemoryView.WordWrap = false;
            // 
            // ObjectPicker
            // 
            this.ObjectPicker.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.HistoryList;
            this.ObjectPicker.FormattingEnabled = true;
            this.ObjectPicker.Location = new System.Drawing.Point(67, 5);
            this.ObjectPicker.Name = "ObjectPicker";
            this.ObjectPicker.Size = new System.Drawing.Size(488, 21);
            this.ObjectPicker.TabIndex = 3;
            this.ObjectPicker.SelectedValueChanged += new System.EventHandler(this.ObjectPicker_SelectedValueChanged);
            this.ObjectPicker.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ObjectPicker_KeyUp);
            // 
            // ReloadButton
            // 
            this.ReloadButton.BackColor = System.Drawing.Color.Transparent;
            this.ReloadButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ReloadButton.BackgroundImage")));
            this.ReloadButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ReloadButton.Image = ((System.Drawing.Image)(resources.GetObject("ReloadButton.Image")));
            this.ReloadButton.Location = new System.Drawing.Point(561, 4);
            this.ReloadButton.Name = "ReloadButton";
            this.ReloadButton.Size = new System.Drawing.Size(23, 23);
            this.ReloadButton.TabIndex = 4;
            this.ReloadButton.UseVisualStyleBackColor = false;
            this.ReloadButton.Click += new System.EventHandler(this.ReloadButton_Click);
            // 
            // bprComboBox
            // 
            this.bprComboBox.FormattingEnabled = true;
            this.bprComboBox.Items.AddRange(new object[] {
            "1",
            "2",
            "4",
            "8",
            "16",
            "32",
            "64"});
            this.bprComboBox.Location = new System.Drawing.Point(646, 5);
            this.bprComboBox.Name = "bprComboBox";
            this.bprComboBox.Size = new System.Drawing.Size(142, 21);
            this.bprComboBox.TabIndex = 6;
            this.bprComboBox.Text = "16";
            this.bprComboBox.SelectedValueChanged += new System.EventHandler(this.bprComboBox_SelectedValueChanged);
            // 
            // ColumnsLabel
            // 
            this.ColumnsLabel.AutoSize = true;
            this.ColumnsLabel.Location = new System.Drawing.Point(590, 8);
            this.ColumnsLabel.Name = "ColumnsLabel";
            this.ColumnsLabel.Size = new System.Drawing.Size(50, 13);
            this.ColumnsLabel.TabIndex = 8;
            this.ColumnsLabel.Text = "Columns:";
            // 
            // MemoryReader
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.ColumnsLabel);
            this.Controls.Add(this.bprComboBox);
            this.Controls.Add(this.ReloadButton);
            this.Controls.Add(this.ObjectPicker);
            this.Controls.Add(this.MemoryView);
            this.Controls.Add(this.AddressLabel);
            this.MinimumSize = new System.Drawing.Size(816, 489);
            this.Name = "MemoryReader";
            this.Text = "MemoryReader";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label AddressLabel;
        private System.Windows.Forms.RichTextBox MemoryView;
        private System.Windows.Forms.ComboBox ObjectPicker;
        private System.Windows.Forms.Button ReloadButton;
        private System.Windows.Forms.ComboBox bprComboBox;
        private System.Windows.Forms.Label ColumnsLabel;
    }
}

